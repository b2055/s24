// 2

db.users.find(

{ 

    $or: [{firstName: {$regex: "s",  $options: "$i"}}, {lastName: {$regex: "d",  $options: "$i"}}]

},

{

    _id: 0,

    firstName: 1,

    lastName: 1

}

)

// 3

db.users.find(
{ 
    $and: [{department: "HR"}, {$or: [{age: {$gt:70}}, {age:70}]}]
}
)

// 4

db.users.find(
{ 
    $and: [{firstName: {$regex: "e",  $options: "$i"}}, {$or: [{age: {$lt:30}}, {age:30}]}]
}
)